# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-perfect-scrollbar/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-perfect-scrollbar"
  spec.version       = RailsAssetsPerfectScrollbar::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "Minimalistic but perfect custom scrollbar plugin"
  spec.summary       = "Minimalistic but perfect custom scrollbar plugin"
  spec.homepage      = "http://noraesae.github.io/perfect-scrollbar"
  spec.license       = "MIT"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
